# Littlefork Core

The machinery for a sequential batch processing pipeline. We use it for
iterative data retrievals and transformations.

## Installation

    npm install --save littlefork-core

## About

Littefork is a product of the
[Exposing the Invisible](https://exposingtheinvisible.org) team.

The best way to learn how to use it to follow the
[tutorial](docs/tutorial.md). If you rather want to develop plugins for
Littlefork, look at the [developers guide](docs/developers-guide.md) and the
[API guide](docs/api.md).

Littlefork is licensed under the [GPL3](LICENSE).

## Development

Please see the [contribution policy](Contributing.md) for more information on
how to contribute.

This package has the following script targets:

-   `watch` - Run a watcher for the tests.
-   `lint` - Use [ESLint](https://eslint.org/) and
    [Prettier](https://github.com/prettier/prettier) to enforce the coding
    style.
-   `fix` - Automatically fix linting errors in the JavaScript code.
-   `clean` - Remove all compiled bundles.
-   `docs` - Build the API docs using
    [Documentation](https://github.com/documentationjs/documentation).
-   `compile` - Compile the ES6 sources using
    [Babel](https://babeljs.io/). Runs the `clean` target before compilation.
-   `build` - Build the whole bundle. This lints, tests, documents and compiles
    the whole package.
-   `check` - Test that ESLint and Prettier are in alignment.
-   `coverage` - Show the code coverage stats for the tests.
-   `publish` - Create a signed [Git
    tag](https://git-scm.com/book/en/v2/Git-Basics-Tagging) and publish to the
    GIT origin remote and the [NPM repository](https://www.npmjs.com/).

## Benchmarks

To run the following benchmarks:b, cd into `./benchmarks` and run:

- `npm run bench:filter` - a custom implementation of `data.filter` and
  homonym.filter are replaced with lodash's `filter`, which is a blast
  compared to legacy.
- `npm run bench:equals` - a better performing version of `data.equals`, that
  is also in use for homonyms. Performs much better than legacy, especially
  when there are more elements to compare.
- `npm run bench:concat` - Measure the performance of `concatManyWith`.
