import {map, merge} from "lodash/fp";
import benchmark from "benchmark";
import {data as d, utils, test} from "littlefork-core";

const {dataId, concatOne} = d;
const {concatManyWith} = utils;
const {generators} = test;

const dataOne = generators.data(1);
const dataFifty = generators.data(50);
const dataHundred = generators.data(100);
const dataOneHashed = map(u => merge(u, {_lf_id_hash: dataId(u)}), dataOne);
const dataFiftyHashed = map(u => merge(u, {_lf_id_hash: dataId(u)}), dataFifty);
const dataHundredHashed = map(
  u => merge(u, {_lf_id_hash: dataId(u)}),
  dataHundred
);

console.log("Finished creating the data"); // eslint-disable-line no-console

const suite = new benchmark.Suite();

suite
  .add("concatManyWith 50/1", () =>
    concatManyWith(dataId, concatOne, dataFifty, dataOne)
  )
  .add("concatManyWith hashed 50/1", () =>
    concatManyWith(dataId, concatOne, dataFiftyHashed, dataOneHashed)
  )
  .add("concatManyWith 50/50", () =>
    concatManyWith(dataId, concatOne, dataFifty, dataFifty)
  )
  .add("concatManyWith hashed 50/50", () =>
    concatManyWith(dataId, concatOne, dataFiftyHashed, dataFiftyHashed)
  )
  .add("concatManyWith 100/50", () =>
    concatManyWith(dataId, concatOne, dataHundred, dataFifty)
  )
  .add("concatManyWith hashed 100/50", () =>
    concatManyWith(dataId, concatOne, dataHundredHashed, dataFiftyHashed)
  )
  .add("concatManyWith 100/100", () =>
    concatManyWith(dataId, concatOne, dataHundred, dataHundred)
  )
  .add("concatManyWith hashed 100/100", () =>
    concatManyWith(dataId, concatOne, dataHundredHashed, dataHundredHashed)
  )
  .on("cycle", ev => console.log(String(ev.target))) // eslint-disable-line no-console
  .on("error", e => console.error(e.target.error)) // eslint-disable-line no-console
  .run();
