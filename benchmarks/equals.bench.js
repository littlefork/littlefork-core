import {reduce, range, concat, isEqualWith} from "lodash/fp";
import benchmark from "benchmark";
import {data as d, test} from "littlefork-core";
import legacyEquals from "./legacy-equals";

const {equals, equalsOne} = d;
const {generators} = test;

const dataOne = generators.data(1);
const dataTwoHundredFifty = generators.data(250);
const dataFiveHundred = generators.data(500);
const data999 = generators.data(999);
const dataThousand = generators.data(1000);
const dataFiftyThousand = reduce(concat(dataThousand), [], range(0, 50));

console.log("Finished creating the data"); // eslint-disable-line no-console

const suite = new benchmark.Suite();

suite
  .add("equals current 1/1", () => equals(dataOne, dataOne))
  .add("equals legacy 1/1", () => legacyEquals(dataOne, dataOne))
  .add("equals lodash 1/1", () => isEqualWith(equalsOne, dataOne, dataOne))
  .add("equals current 500/1", () => equals(dataFiveHundred, dataOne))
  .add("equals legacy 500/1", () => legacyEquals(dataFiveHundred, dataOne))
  .add("equals lodash 500/1", () =>
    isEqualWith(equalsOne, dataFiveHundred, dataOne)
  )
  .add("equals current 500/250", () =>
    equals(dataFiveHundred, dataTwoHundredFifty)
  )
  .add("equals legacy 500/250", () =>
    legacyEquals(dataFiveHundred, dataTwoHundredFifty)
  )
  .add("equals lodash 500/250", () =>
    isEqualWith(equalsOne, dataFiveHundred, dataTwoHundredFifty)
  )
  .add("equals current 500/500", () => equals(dataFiveHundred, dataFiveHundred))
  .add("equals legacy 500/500", () =>
    legacyEquals(dataFiveHundred, dataFiveHundred)
  )
  .add("equals lodash 500/500", () =>
    isEqualWith(equalsOne, dataFiveHundred, dataFiveHundred)
  )
  .add("equals current 1000/999", () => equals(dataThousand, data999))
  .add("equals legacy 1000/999", () => legacyEquals(dataThousand, data999))
  .add("equals lodash 1000/999", () =>
    isEqualWith(equalsOne, dataThousand, data999)
  )
  .add("equals current 1000/1000", () => equals(dataThousand, dataThousand))
  .add("equals legacy 1000/1000", () =>
    legacyEquals(dataThousand, dataThousand)
  )
  .add("equals lodash 1000/1000", () =>
    isEqualWith(equalsOne, dataThousand, dataThousand)
  )
  .add("equals current 50000/1000", () =>
    equals(dataFiftyThousand, dataThousand)
  )
  .add("equals lodash 50000/1000", () =>
    isEqualWith(equalsOne, dataFiftyThousand, dataThousand)
  )
  .on("cycle", ev => console.log(String(ev.target))) // eslint-disable-line no-console
  .on("error", e => console.error(e.target.error)) // eslint-disable-line no-console
  .run();
