import {curry, zip, every, sortBy, size, isEqual} from "lodash/fp";
import {data as d} from "littlefork-core";

const {equalsOne, dataId} = d;

export default curry((xs, ys) => {
  const elems = zip(sortBy(dataId, xs), sortBy(dataId, ys));
  return (
    isEqual(size(xs), size(ys)) && every(([x, y]) => equalsOne(x, y), elems)
  );
});
