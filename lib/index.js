import {merge} from "lodash/fp";

import u from "./utils";
import plugins from "./utils/plugins";
import hasher from "./utils/hasher";
import assertions from "./utils/assertions";
import combinators from "./utils/combinators";
import fs from "./utils/fs";
import generators from "./test/generators";

export {default as runner} from "./runner";
export {default as envelope} from "./data/envelope";
export {default as queries} from "./data/list";
export {default as data} from "./data/data";
export {default as plugin} from "./data/plugin";
export const utils = merge(u, {plugins, hasher, assertions, combinators, fs});
export const test = {generators};
